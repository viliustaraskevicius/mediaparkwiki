## Symfony


* [Coding styles and rules](symfony-coding-styles-and-rules.md)
* [Translations](symfony-translations.md)
* [Useful symfony bundles](symfony-useful-bundless.md) 