Here we will dump information regarding our workflow, best practices, etc.

* [IDE](ide.md)
* [Version control system (VCS)](version-control-system.md) 
* [PHP](php.md) 
* [Front-end](front-end.md)
* [Servers](server-configuration.md)
* [APPS](apps.md)
* [TODO](todo.md)
* [Documentation](documentation.md)
* [Other](other.md)
