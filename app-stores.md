General info about managing Google Play, iTunes and Apple Developer accounts
==================

This guide explains basic things how to manage app store accounts for project manager. If you can't find required info below - ask vilius@mediapark.com

FAQ
-----------

### Which account should I use to publish new app?

Common Mediapark practice is to publish apps on client Google Play/Apple developer account.

### What if client doesn't have Google Play/Apple developer account.

Project manager should take care that this account will be created. Start creating accounts at the beginning of project, because creating iTunes account can take more than a month.

**Important:** New Apple developer account must be created as company account, not individual person, because later you won't be able to add more users.

### How much does it cost to create app store accounts?
- Apple developer - 99$/year
- Google Play - 25$ one time fee

### Where to start if I need to create new Google Play/Apple developer account?
- Google Play - http://play.google.com/apps/publish/
- Apple Developer - https://developer.apple.com/enroll/


### What info do I need before I can publish Android/iOS app?
- Title
- Short description (Google Play up to 80 symbols, iTunes up to 170 symbols)
- Full description (up to 4000 symbols)
- Screenshots (iTunes up to 5, Android up to 8).
Additional requirements for iTunes:
  - iPhone - 1242 x 2208 / 2208 x 1242
  - iPad - 2048 x 2732 / 2732 x 2048
  - Can't containt alpha channel (transparency)
- Icon (Android 512x512, iOS 1024x1024)
- Feature graphic (only for Android, 1024x500)
- Contact email
- Keywords (only iOS, up to 100 symbols)
- Copyright info
- Website address
- Link to privacy policy (only Android)


### How to manage Google Play apps?
You can find all the in [Google Play Console Help Center].

[Google Play Console Help Center]: https://support.google.com/googleplay/android-developer/?hl=en


### How to mange iTunes apps?
You can find all the info in [iTunes Connect Developer Guide]

[iTunes Connect Developer Guide]: https://developer.apple.com/library/content/documentation/LanguagesUtilities/Conceptual/iTunesConnect_Guide/Chapters/About.html#//apple_ref/doc/uid/TP40011225-CH1-SW1