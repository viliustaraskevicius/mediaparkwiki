## Postfix configuration

#### default_extra_recipient_limit = 10
Limit the number of recipients of each message. If a message had 20 recipients on the same domain, postfix will break it out to two different email messages instead of one.

#### default_destination_rate_delay = 2s
Postfix will add a delay between each message to the same receiving domain. It overrides the previous rule and in this example, it will send one email after another with a delay of 1 second. If you want to disable this rule, either delete it or set to 0.

#### maximal_queue_lifetime = 1h (default 5d)
Consider a message as undeliverable, when delivery fails with a temporary error, and the time in the queue has reached the maximal_queue_lifetime limit.
5 days in queue? No no no, if it is not delivered in 1h - it is not deliverable.

#### maximal_backoff_time = 15m (default 4000s)
try to send deferred message every 15 minutes, and consider it as undeliverable if the message reaches maximal_queue_lifetime.

#### minimal_backoff_time = 5m

#### queue_run_delay = 5m (if default is 1000s (> postfix 2.4 - default is 1000s))
scans deferred queue every 5minutes.

** TODO: improvement **


leidzia siusti tik is localhost
inet_interfaces = 127.0.0.1


smtpd_sender_restrictions =
   check_sender_access regexp:/etc/postfix/access_sender

/etc/postfix/access_sender:
    !/^info@sweepest.com$/ REJECT // rejectina viska kas yra ne is info@sweepest.com

postmap /etc/postfix/access_sender
service postfix restart
    

smtp_destination_concurrency_limit = 2
smtp_destination_rate_delay = 1s
smtp_extra_recipient_limit = 10

//https://www.e-rave.nl/rate-limit-incoming-mail-on-postfix
smtpd_error_sleep_time = 1s
smtpd_soft_error_limit = 10
smtpd_hard_error_limit = 20


dovecot off

swiftmailer naudoti transport type sendmail - tada galima isjungt postfixo smtp