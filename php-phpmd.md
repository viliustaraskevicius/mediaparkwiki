# PHP Mess Detector

## Installation
* Refer to PHPMD documentation https://phpmd.org/documentation/index.html
```composer global  require phpmd/phpmd=*```
* Install extended PHPMD version
```composer global  require  mi-schi/phpmd-extension```

## Usage

### PHPStorm configuration:
* Download [PHPMD Extended Rules](resources/phpmd.xml)
* Set custom ruleset on PHPStorm:
`File > Settings > Editor > Inspections > PHP Mess detector validation > Custom rulesets` and point to downloaded ruleset.
* Do not forget to enable plugin.
  https://www.jetbrains.com/help/phpstorm/mess-detector.html

__Note__: It is possible tu run PHPMD manually:
```
~/.composer/vendor/bin/phpmd www/src/FormBuilderBundle/Service/  xml  ~/Documents/projects/MediaparkWiki/resources/ruleset.xml
```
