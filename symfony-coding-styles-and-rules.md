Coding styles and rules
==================        
1. Services
--------------
### 1.1. Services naming

* Service should starts with bundle alias (e.g. in AppBundle services starts app.service_name)
* If service for entity logic it should start with entity name and keyword `service` (e.g. AppBundle\Entity\User, service app.user_service)

### 1.2 Service path

* Services should be in `Service` folder in bundle

2. Utils
--------------
### 2.1. Utils naming

* Utils must starts with Enitity name or business logic entity naming and end with `Utils` e.g.: StringUtils, UserUtils

### 2.2. When to create utility class ?

* In a good object-oriented design, most classes should represent a single thing and all of it is attributes and operations, so in utility class also. Utilis class should represent a one logic e.g. StringUtils should have only logic aboud strings.

### 2.3. How to create utility class?

* Recommended create utility  class without DI objects and create this class in static state.

3. Repositories
--------------
### 3.1 Repository methods

* Repository names must always start with `find` prefix. In cases where you need to return `QueryBuilder` object - use `get` or `create` prefix. 
* Repository methods should not contain any extra logic (foreaching the results array, modifying results) - if needed - create a custom hydrator (it is hard to implement, so better do the logic in service class).