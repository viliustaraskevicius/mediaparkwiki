## PHP nice to have/must be

 - Vagrant (https://mediapark.githost.io/internal/vagrant-symfony-3)
 - CodeSniffer + https://github.com/djoos/Symfony2-coding-standard
 - Deployment scripts (https://deployer.org/)
 - PHP Array short syntax -> []
 - Class name resolution via Class::class
 - DB tables must have created_at, updated_at fields (https://packagist.org/packages/stof/doctrine-extensions-bundle)
 - Must use https://packagist.org/packages/doctrine/doctrine-migrations-bundle
 - Use events for business logic 