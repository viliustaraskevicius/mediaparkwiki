Project title
========================

Short description

`Last updated: Y-m-d`

Requirements
========================

  - PHP 7.0
  - Apache/Nginx
  - MySQL
  - ...

Development environment
========================

1. cd project directory
2. set rwx permissions `./var/cache ./var/logs ./web/uploads`  [Setting up or Fixing File Permissions](http://symfony.com/doc/2.8/setup/file_permissions.html)
3. ...

Development notes(optional)
========================

* Some information.
* ...

Deployment
========================

* Project uses [Deployer](https://deployer.org/) as auto-deployment tool.
    - staging: `php vendor/deployer/deployer/bin/dep deploy staging` 
    - prod: `php vendor/deployer/deployer/bin/dep deploy prod`
    - ...

Cron jobs
========================

For production env only.
```
*/60 * * * * ./cronjob-01
...
```

Daemon scripts
========================

For production env only.
```
./script-name
...
```

Tests
========================

1. run `./bin/phpunit -c app --colors -c ./phpunit.xml.dist`
2. ...