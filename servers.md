## Servers

#### iv.lt

109.235.65.133 - 570564.s.dedikuoti.lt

194.135.86.179 - cili-be.mediapark.lt

94.176.233.211 - delta.mediapark.lt

79.98.31.82 - dev.mediapark.lt

185.5.55.49 - staging.mediapark.lt

79.98.31.88 - test.mediapark.lt

109.235.70.49 - server.etpa.lt

194.135.93.20 - alfa.mediapark.lt
 * [**autobid**][autobid-web]

185.69.55.174 - gama.mediapark.lt

195.181.243.136 - omega.mediapark.lt

194.135.91.105 - sigma.mediapark.lt (KVM)

79.98.25.167 - rinkimai-be.mediapark.lt

194.135.86.217 - 577544.s.dedikuoti.lt

94.176.235.248 - 589983.s.dedikuoti.lt

194.135.87.109 - ergoapp.lt

194.135.87.66 - kuriamerespublika.lt

79.98.29.4 - taikomojikalbotyra.lt

80.208.230.206 - mokejimai.musukrepsinis.lt
 * [**LKF mokejimai**][lkf-web]

[lkf-web]: https://mediapark.githost.io/lkf/web
[autobid-web]: https://mediapark.githost.io/autobid/web

#### aws 

// TODO