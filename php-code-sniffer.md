# Code Sniffer

## Installation
* Refer to code sniffer installation instructions on https://github.com/squizlabs/PHP_CodeSniffer and install code sniffer globally.
```composer global require "squizlabs/php_codesniffer=*"```
* Install Symfony2 standard globally: 
```composer global  require escapestudios/symfony2-coding-standard:3.x-dev```
* Include new standard:
```~/.composer/vendor/bin/phpcs --config-set installed_paths  ~/.composer/vendor/escapestudios/symfony2-coding-standard```
* Check "Symfony" standard is installed:
```~/.composer/vendor/bin/phpcs -i```

## Usage
### PHPStorm configuration:
* Select "Symfony" rules on PHPStorm 
```
File > Settings > Editor > Inspections > PHP Code Sniffer validation > Codding standard
```
* Do not forget to enable plugin. https://confluence.jetbrains.com/display/PhpStorm/PHP+Code+Sniffer+with+Symfony2+Coding+Standards+Integration+in+PhpStorm+-+Symfony+Development+using+PhpStorm


__Note:__
* `phpcs` detects PHP, JS, CSS issues.
* `phpcbf` fixes issues (Code beautifier and fixer).
* Inspect code manually
```~/.composer/vendor/bin/phpcs ./www/src/```
