## Development environment setup

#### We recommend to use a docker

 - https://mediapark.githost.io/internal/docker-symfony-3
 
### Also you can use a vagrant(not recommended)

 - https://mediapark.githost.io/internal/vagrant-symfony-3