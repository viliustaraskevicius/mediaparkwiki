## TODO


* Regular solution setup for C# projects? How we call solution projects, how we use git (strategy / branching / feature branches / merge into dev / master for production), how we configure TC for testing, etc
* Teamcity location, introduction, whom to ask for access, how to configure git/TC for notifications, TC cheatsheet. TC for testing, TC for deployment
* access to gitlab through same google authentication we use for work? that would be awesome.
* trello usage strategy short description. Estimating in points, tagging tasks, discussions, etc
* info on where to get M$ software from - we got MSDN BizSpark with licensing, n00bs should know they can ask for licenses for their dev machines