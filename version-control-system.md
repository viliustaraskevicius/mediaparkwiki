## Version control system (VCS)

For source code management is used <a href="https://git-scm.com/">Git</a> version control system.


#### The branches naming

The branches of new features SHOULD be begins with `features` prefix.

Example of Paypal payment branch name:

```
feature/paypal-payment
```

The hotfixes branches SHOULD be begins with `hotfix` prefix.

Example of header height fixes branch name:

```
hotfix/header-height-fixes
```

The words in branches names SHOULD be separated by `-` dash.

If the project uses Trello task board, it is recommended add Trello task ID to branch name (after prefix).

```
feature/107-paypal-payment
```

####  Git clients with GUI

It is strongly recommended to use SourceTree Git client https://www.sourcetreeapp.com/.