## PHP

* [Coding styles and rules](php-coding-style.md) 
  * [PHP code sniffer](php-code-sniffer.md)
  * [PHP mess detector](php-phpmd.md)
* [PHP nice to have/must be](php-nice-to-have-must-be.md) 
* [Useful PHP packages](php-useful-packages.md) 
* [PHP Frameworks](php-frameworks.md)
* [Development environment](php-dev-env.md)