## How to create project

## Gitlab user
1. All project users must be registered by unique name (copy&paste from Timebase)
    * Username: First name from Timebase (if first name is not unique, create with last name: name.last_name)
    * Email: copy&paste from Timebase

## Testing environment
1. Testing code must be in the gama.mediapark.lt server (/home/gama) with **gama** user
2. CI from gitlab should work in master branch
