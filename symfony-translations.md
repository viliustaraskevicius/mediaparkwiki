## Symfony translations


This part describes how to be organized translations in new Symfony projects.

#### The global translations

The global translations SHOULD be placed in 'app\Resources\translations\messages.locale.yml' file.


#### The bundle translations

Each bundle can have their translations. The bundle translations SHOULD be placed in 'Resources\translations\messages.locale.yml' bundle file.


#### The messages.locale.yml file

The translations SHOULD be stored in YAML format. The multiwords keys of translations SHOULD be concatenated with underscores.

```
title:
    coupon_packs: Coupon packs
form:
    label:
        name: Name
warning:
    coupon_packs_cannot_be_deleted: The coupon packs with used coupons cannot be deleted:
    coupon_packs_deleted: The coupon packs successfully deleted.
```
