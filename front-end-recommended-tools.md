# Recommended Front-end tools

Front end / app developers recommends to stick to these 4 tools:
1. NPM. Package manager. Can run simple scripts.
1. Webpack. Code bundler. Can execute most code related tasks. Initially it is difficult to configure.
1. Gulp. Task runner. Used in case there is need for complex commands and NPM is not enough. 
There is a risk to get stuck with unsupported plugins. Also debugging can be difficult.
1. Angular4. FE framework.

Outdated / rarely recommended tools:
1. Bower 
1. Grunt
1. Rollup
1. Browserify
